---
layout: portfolio
permalink: /portfolio/
title: Portfolio
sub: Sorry, the page is under construction :(
foot:
  name: What do you think?
  desc: Thanks for spending time to read through my website.<br/>You are truly amazing! The site is still under construction here and there. My apologies if you stumbled upon empty pages with dummy text.
  link: "https://goo.gl/NbkkYq"
---
