---
# You don't need to edit this file, it's empty on purpose.
# Edit theme's home layout instead if you wanna make some changes
# See: https://jekyllrb.com/docs/themes/#overriding-theme-defaults
layout: index
intro:
  title: Hello, I'm Van Nhi
  content: "You can call me Nhi for short! I'm a third-year student studying computer science (majoring in software development) at Swinburne University. I enjoy finding the best solution to problems, mucking around with programming languages and documenting issues I came across and how I fixed them."
  button:
    link: https://blog.nhivn.dev
    text: Check out my blog &nbsp;&#9749;

first:
  title: I have used
  features:
  - name: "C#"
    icon: devicon-csharp-plain
    clr: style1
    link: "https://docs.microsoft.com/en-us/dotnet/csharp/csharp"
  - name: "HTML"
    icon: devicon-html5-plain
    clr: style2
    link: ""
  - name: "Sass"
    icon: devicon-sass-original
    clr: style3
  - name: "Git"
    icon: devicon-git-plain
    clr: style4
  - name: "Javascript"
    icon: devicon-javascript-plain
    clr: style5
  - name: "PHP"
    icon: devicon-php-plain
    clr: style1
  - name: "Android"
    icon: devicon-android-plain
    clr: style2
  - name: "MySQL"
    icon: devicon-mysql-plain
    clr: style3
  - name: "Java"
    icon: devicon-java-plain
    clr: style4
  - name: "Jetbrains"
    icon: devicon-jetbrains-plain
    clr: style5
second:
  title: I have worked as
  sub: Blah blah blah blah blah blah
  exp:
  - time: Nov 2018 - Present
    pos: Software Engineer
    comp: Compass Education
    duties:
    - desc: Perform code reviews and assist new developers in familiarising themselves with the Compass codebase and architecture.
    - desc: Entrusted with time-sensitive and tender-related projects.
    - desc: Oversee the team if the team leader is not in the office.
    achi:
    - desc: Well regarded by colleagues and direct manager.
  - time: Nov 2017 - Nov 2018
    pos: Junior Software Developer
    comp: Compass Education
    duties:
    - desc: Collaborate with a team of developers to create good user experiences using technologies such as HTML, CSS, Javascript, C#.
    - desc: Prototype new features for the websites then implement and test them using object-oriented programming principles before delivering the final result to be released in production.
    - desc: Autonomously identify areas for optimisation and develop solutions to software issues.
    achi:
    - desc: Promoted to be a software engineer.
  - time: Feb - April 2016
    pos: Teaching assistant
    comp: British Education Partnerships
    duties:
    - desc: Co-work with English teachers in kindergarten and secondary schools.
    - desc: Manage class attendance and report to the office.
    - desc: Assist TA supervisors with office paperwork.
    achi:
    - desc: Complimented by the supervisors, English teachers and event manager.
  - time: Jan - Mar 2016
    pos: Sales representative
    comp: Gau Uniform
    duties:
    - desc: Search for potential customers and sign contracts with them.
    - desc: Run business campaigns and promote products from the company.
    achi:
    - desc: Contribute a total amount of 14 million VND to the company’s turnover.
  - time: Aug - Nov 2015
    pos: Local supporter
    comp: Projects Aboard
    duties:
    - desc: Assist in Phuc Tue Center for Autisms with three young European volunteers.
    - desc: Teach young patients in Thalassemia, Hemophilia and Leukemia departments.
    - desc: Support acupuncturists and young patients in National Hospital of Acupuncture.
    achi:
    - desc: Develop new methods to teach children boosting work efficiency.
    - desc: Manage volunteers’ schedule and arrange economical city tour that contribute to good customer service.
    - desc: Complimented by supervisors and doctors of National Institute of Hematology and Blood Transfusion.
third:
  title: In my free time, I do
  sub: all sorts of activities such as ...
  desc: ""
  hobbies:
  - name: Volunteering
    icon: fa-gratipay
    clr: style1
    link: "#"
  - name: Design
    icon: fa-behance
    clr: style2
    link: "https://www.behance.net/kiwixiwie9385"
  - name: Photography
    icon: fa-camera-retro
    clr: style3
    link: ""
  - name: Travelling
    icon: fa-map
    clr: style4
    link: ""
  - name: Reading
    icon: fa-book
    clr: style5
    link: "https://www.goodreads.com/user/show/55847107-nhi"
foot:
  name: Wow, you're here!
  desc: Thanks for spending time to read through my website.<br/>You are truly amazing! The site is still under construction here and there. My apologies if you stumbled upon empty pages with dummy text.
  link: "https://goo.gl/NbkkYq"
---
